FROM osrf/ros:kinetic-desktop-full

# Change the default shell to Bash
SHELL [ "/bin/bash" , "-c" ]

# Install packages
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' \
&& apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 \
&& apt-get update \
&& apt-get install -y \
nano \
# Clear apt-cache to reduce image size
&& rm -rf /var/lib/apt/lists/*
ENV RAS_WS=/root/ras_ws
RUN mkdir -p $RAS_WS/src
WORKDIR $RAS_WS

# Initialize local catkin workspace
RUN source /opt/ros/${ROS_DISTRO}/setup.bash \
# Update apt-get because its cache is always cleared after installs to keep image size down
&& apt-get update \
# Install dependencies
&& cd $RAS_WS \
# Build catkin workspace
catkin_make