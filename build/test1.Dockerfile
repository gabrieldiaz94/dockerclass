FROM ubuntu:18.04

SHELL [ "/bin/bash" , "-c" ]
RUN apt-get -y update && apt-get install -y nano &&  rm -rf /var/lib/apt/lists/*
#RUN groupadd -r user && useradd -r -g user user
#USER user

COPY app app/

ARG RAS=RASCHAPTER
RUN echo "Hola amigos bienvenidos a $RAS" > app/ras.txt

CMD cat app/ras.txt